#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 31 23:03:37 2020
@author: stem
"""
#%%
import numpy as np
from netCDF4 import Dataset
import os
from datetime import timedelta

# Own functions:
from namelist import *

#%%
def readFLIGHT(flight_date, flight_start, flight_end, mission, flight_nr):
    print("---------------------------------------------------------")
    print("Reading measured data from %s-flight..." % (mission))
    infile1 = "_%s_%s_romeo-reanalysis-002_%s" % (
        mission,
        flight_date.strftime("%Y-%m-%d"),
        flight_date.strftime("%Y%m%d"),
    )
    infile2 = "_%s_%s_romeo-reanalysis-002_%s" % (
        mission,
        flight_date.strftime("%d%m%Y"),
        flight_date.strftime("%Y%m%d"),
    )
    if flight_date.strftime("%d%m%Y") == "03102019" and mission == "INCAS":
        infile2 = "_%s_%i-%s_romeo-reanalysis-002_%s" % (
            mission,
            flight_nr,
            flight_date.strftime("%d%m%Y"),
            flight_date.strftime("%Y%m%d"),
        )
    for flightfile in os.listdir(flightdir):
        if infile1 in flightfile or infile2 in flightfile:
            infile = flightfile
    fname = os.path.join(flightdir, infile)
    fh = Dataset(fname, mode="r")

    dicts_flight = {}
    date_obs = []
    if mission == "SA":
        varlist = SA_vars
    if mission == "INCAS":
        varlist = INCAS_vars
    ###Create date from epoch-time:
    ftime = np.array(fh.variables["time"][:])
    for isec in ftime:
        date_obs = np.append(date_obs, date_ref + timedelta(seconds=int(isec)))
    if not flight_start:
        flight_start = date_obs[0]
    if not flight_end:
        flight_end = date_obs[-1]
    # Add lat/lon:
    varlist.append("lon")
    varlist.append("lat")
    for var in varlist:
        dicts_flight.__setitem__(
            var,
            np.array(
                fh.variables[var][
                    (date_obs > flight_start) & (date_obs < flight_end)
                ]
            ),
        )
    dicts_flight.__setitem__(
        "date_obs",
        date_obs[(date_obs > flight_start) & (date_obs < flight_end)],
    )
    dicts_flight.__setitem__("h", 0.001 * np.array(fh.variables["h"]))
    if mission == "INCAS":
        dicts_flight["flight_altitude"] = dicts_flight.pop("flight_altitude_sl")
    dicts_flight = np.array([dicts_flight])

    return dicts_flight
