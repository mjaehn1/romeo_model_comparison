#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 08:37:13 2020
@author: stem
"""
#%%
import os
import numpy as np
from bs4 import BeautifulSoup
from shapely.geometry import Polygon
import xlrd
import cartopy.crs as ccrs
from namelist import *

#%%
def readPOLY():
    dicts_poly = []
    for kml_f in kml_list:
        kml_file = os.path.join(kml_path, kml_f)
        ###Open kml-files:
        f = open(kml_file, "r")
        s = BeautifulSoup(f, "xml")
        ###Read names of polygons:
        if kml_f in ["C6_east.kml", "C6_north.kml", "C7_northwest.kml"]:
            ###The 3 files that we defined have a shorter namelist:
            polygons = s.find_all("name")[1:]
        else:
            ###The other files, defined by T. Röckmann:
            polygons = s.find_all("name")[2:]
        ###Read coordinates (edges):
        coords = s.find_all("coordinates")
        ###Go through all polygons in the file:
        for ipol, poly in enumerate(polygons):
            ###Select the coordinates (edges) of this polygon:
            xml_coord = coords[ipol]
            ###Split the string...
            coord_split = str(xml_coord).split()
            ###...and read the numbers (still string):
            coord_table = coord_split[1:-1]
            ###Initialize array for edges of polygon:
            edges = np.zeros((len(coord_table), 2))
            ###Go over all edge-points:
            for iedge in np.arange(len(coord_table)):
                ###Transform to float:
                clus_lon = float(coord_table[iedge].split(",")[0])
                clus_lat = float(coord_table[iedge].split(",")[1])
                edges[iedge, 0] = clus_lon
                edges[iedge, 1] = clus_lat
            ###Create polygon with edges:
            poly_cluster = Polygon(edges)
            poly_name = polygons[ipol]
            dicts_poly = np.append(
                dicts_poly, {"polygon": poly_cluster, "name": poly_name}
            )

    return dicts_poly


#%%
def readWELLS():
    """Load OMV-wells and -facilities"""
    dicts_omv = []
    ###Open Workbook
    wb_wells = xlrd.open_workbook(wellfile)
    wb_facil = xlrd.open_workbook(facfile)
    sheet_wells = wb_wells.sheet_by_index(0)
    sheet_facil = wb_facil.sheet_by_index(0)
    ###########################################################################
    """Read active wells:"""
    ###Get Categories:
    cat_counter = 0
    for column in range(sheet_wells.ncols):
        if sheet_wells.cell_value(0, column) == "CURRENT_STATUS":
            stat_idx = column
        elif sheet_wells.cell_value(0, column) == "LEGAL_CATEGORY_NAME":
            cat_idx = column
        elif sheet_wells.cell_value(0, column) == "TOP_LONGITUDE":
            lon_idx = column
        elif sheet_wells.cell_value(0, column) == "TOP_LATITUDE":
            lat_idx = column
    ###Read all wells:
    for iline, line in enumerate(range(sheet_wells.nrows)):
        status = sheet_wells.cell_value(line, stat_idx)
        cat = sheet_wells.cell_value(line, cat_idx)
        lon = sheet_wells.cell_value(line, lon_idx)
        lat = sheet_wells.cell_value(line, lat_idx)
        ###Only wells with status "Production":
        if status == "Production":
            ###If coordinate-entry exists:
            if type(lon) == float:
                rlon, rlat = rotpole2wgs(lon, lat, pollon, pollat, inverse=True)
                dicts_omv = np.append(
                    dicts_omv,
                    {
                        "rlon": lon,
                        "rlat": lat,
                        "type1": "well",
                        "type2": [],
                        "status": status,
                        "cat": cat,
                        "stream": [],
                    },
                )
    ###Get idx of columns to read facilities:
    for column in range(sheet_facil.ncols):
        if sheet_facil.cell_value(0, column) == "FACILITY_STREAM":
            stream_idx = column
        elif sheet_facil.cell_value(0, column) == "FACILITY_TYPE":
            type_idx = column
        elif sheet_facil.cell_value(0, column) == "FACILITY_CURRENT_STATUS":
            stat_idx = column
        elif sheet_facil.cell_value(0, column) == "Longitudine":
            lon_idx = column
        elif sheet_facil.cell_value(0, column) == "Latitudine":
            lat_idx = column
    ###Read facilities:
    for line in range(sheet_facil.nrows):
        stream = sheet_facil.cell_value(line, stream_idx)
        factype = sheet_facil.cell_value(line, type_idx)
        status = sheet_facil.cell_value(line, stat_idx)
        lon = sheet_facil.cell_value(line, lon_idx)
        lat = sheet_facil.cell_value(line, lat_idx)
        ###Only consider operating facilities related to oil and gas_
        if ("Gas" in stream) or ("Oil" in stream):
            if status == "Operating":
                rlon, rlat = rotpole2wgs(lon, lat, pollon, pollat, inverse=True)
                dicts_omv = np.append(
                    dicts_omv,
                    {
                        "rlon": lon,
                        "rlat": lat,
                        "type1": "facility",
                        "type2": factype,
                        "status": status,
                        "cat": [],
                        "stream": stream,
                    },
                )

    return dicts_omv


#%%
def rotpole2wgs(rlon, rlat, pollon, pollat, inverse=False):
    """
    Transform rotated pole to WGS84.
    """
    c_in = ccrs.RotatedPole(pollon, pollat)
    c_out = ccrs.PlateCarree()

    if inverse:
        c_in, c_out = c_out, c_in

    if np.ndim(rlon) == 0:
        res = c_out.transform_point(rlon, rlat, c_in)
        return res[0], res[1]
    elif np.ndim(rlon) in [1, 2]:
        res = c_out.transform_points(c_in, rlon, rlat)
        return res[..., 0], res[..., 1]
    else:
        shape = rlon.shape
        res = c_out.transform_points(c_in, rlon.flatten(), rlat.flatten())
        return res[:, 0].reshape(shape), res[:, 1].reshape(shape)
