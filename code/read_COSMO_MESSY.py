#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 31 21:51:52 2020
@author: stem
"""
#%%
import numpy as np
from netCDF4 import Dataset
import os
import os.path
from datetime import datetime, timedelta
from cdo import Cdo

# Own functions:
from namelist import *

#%%
cdo = Cdo()
#%%
def readDLR(map_height, date_obs, CH4_vars_map):

    filename_t = "ROMEO_ANA_2____%s_000000_tracer_gp.nc" % (
        date_obs[0].strftime("%Y%m%d")
    )
    filename_C = "ROMEO_ANA_2____%s_000000_COSMO.nc" % (
        date_obs[0].strftime("%Y%m%d")
    )
    file_t = os.path.join(messyoutput, filename_t)
    file_C = os.path.join(messyoutput, filename_C)
    fh_t = Dataset(file_t)
    dayfrac = np.array(fh_t.variables["time"])
    model_date = []
    for df in dayfrac:
        model_date = np.append(
            model_date,
            datetime.strptime("2019-09-21T00:00", "%Y-%m-%dT%H:%M")
            + timedelta(days=df),
        )
    # round:
    for idate, mdate in enumerate(model_date):
        rdate = (mdate + timedelta(seconds=10)).replace(
            minute=0, second=0, microsecond=0
        )
        model_date[idate] = rdate
    ii = 0
    enddate = date_obs[-1]
    loopdate = date_obs[0]
    datelist = []
    while loopdate <= enddate:
        loopdate = (date_obs[0] + timedelta(minutes=30)).replace(
            minute=0, second=0
        ) + timedelta(hours=ii)
        if loopdate > (enddate + timedelta(minutes=30)):
            continue
        datelist = np.append(datelist, loopdate)
        ii += 1
    ###############
    print("---------------------------------------------------------")
    print("Remapping COMSO/Messy-output...")
    print("     ...%s" % (date_obs[0].strftime("%Y%m%d")))
    for ts, md in enumerate(model_date):
        if md == datelist[0]:
            ts1 = ts
        if md == datelist[-1]:
            ts2 = ts

    messy_names = []
    for cname in CH4_vars_map:
        messy_names = np.append(messy_names, CH4names[cname]["DLR"])

    cdo.seltimestep(
        "%i/%i" % (ts1, ts2), input=file_t, output="timeselect_t.nc"
    )
    cdo.seltimestep(
        "%i/%i" % (ts1, ts2), input=file_C, output="timeselect_C.nc"
    )
    cdo.intlevel3d(
        "../data/grids/hhl.nc timeselect_t.nc ../data/grids/nc_height.nc",
        output="../data/grids/intheight_t.nc",
    )
    cdo.intlevel3d(
        "../data/grids/hhl.nc timeselect_C.nc ../data/grids/nc_height.nc",
        output="../data/grids/intheight_C.nc",
    )
    # remap to specific grid:
    cdo.remapcon(
        "../data/grids/outgrid.txt -selname,"
        + ",".join(map(str, messy_names))
        + " -setgrid,../data/grids/messy_in_grid.txt",
        input="../data/grids/intheight_t.nc",
        output="regridded_t.nc",
    )
    cdo.remapcon(
        "../data/grids/outgrid.txt -selname,"
        + ",".join(map(str, ["um1_geo", "vm1_geo", "tm1", "rhum"]))
        + " -setgrid,../data/grids/messy_in_grid.txt",
        input="../data/grids/intheight_C.nc",
        output="regridded_C.nc",
    )
    os.remove("timeselect_t.nc")
    os.remove("timeselect_C.nc")
    os.remove("../data/grids/intheight_t.nc")
    os.remove("../data/grids/intheight_C.nc")
    ###############
    print("---------------------------------------------------------")
    print("Reading COMSO/Messy-output...")
    print("     ...%s" % (date_obs[0].strftime("%Y%m%d")))
    """Go through all timesteps and open/read file"""
    fields = []
    filename_t = "regridded_t.nc"
    filename_C = "regridded_C.nc"
    data_t = Dataset(filename_t)
    data_C = Dataset(filename_C)
    ###Read data
    for idate, modeldate in enumerate(datelist):
        CH4_field = np.zeros((data_t.variables["CH4_fx"][0, 0, ...]).shape)
        lat = data_t.variables["lat"][:]
        lon = data_t.variables["lon"][:]
        U = data_C.variables["um1_geo"][idate, 0, ...]
        V = data_C.variables["vm1_geo"][idate, 0, ...]
        for xy_tracer in messy_names:
            CH4_field += 1e6 * np.array(
                data_t.variables["%s" % (xy_tracer)][idate, 0, ...]
            )  ###ppb --> ppm
        fields = np.append(
            fields,
            {
                "CH4": CH4_field,
                "date": modeldate,
                "lat": lat,
                "lon": lon,
                "U": U,
                "V": V,
            },
        )
    os.remove(filename_t)
    os.remove(filename_C)

    return fields


#%%
def readEXTRACTED_DLR(
    model,
    flight_date,
    CH4_staggered,
    CH4_vars_map,
    mission,
    flight_start,
    flight_end,
    flight_nr,
):
    print("---------------------------------------------------------")
    print("Reading extraced DLR model data...")
    if mission == "SA":
        mission = "SCIAVI"
    infile = "ROMEO_ANA_2____%s_" % (flight_date.strftime("%Y%m%d"))
    if flight_date.strftime("%Y%m%d") == "20191003" and mission == "INCAS":
        infile = "ROMEO_ANA_2____%i-%s_" % (
            flight_nr,
            flight_date.strftime("%Y%m%d"),
        )
    #    for flightfile in os.listdir(os.path.join(messycurtain,'s4d_%s'%(mission))):
    #        if infile in flightfile:
    #            infile_c = flightfile
    for flightfile in os.listdir(messyextract):
        if infile in flightfile:
            infile_f = flightfile

    #    fname_c = os.path.join(os.path.join(messycurtain,'s4d_%s'%(mission)),infile_c)
    fname_f = os.path.join(messyextract, infile_f)
    #    fh_c    = Dataset(fname_c,mode='r')
    fh_f = Dataset(fname_f, mode="r")

    dicts_curtain = {}
    global dicts_extracted
    dicts_extracted = {}
    varlist = model_vars_messy
    ###Create date from day-fraction:
    dayfrac = np.array(fh_f.variables["time"])
    date_ext = []
    for df in dayfrac:
        date_ext = np.append(
            date_ext,
            datetime.strptime("2019-09-21T00:00", "%Y-%m-%dT%H:%M")
            + timedelta(days=float(df)),
        )
    if not flight_start:
        flight_start = date_ext[0]
    if not flight_end:
        flight_end = date_ext[-1]
    # Add the tracers from the staggered plot:
    seen = set()
    CH4_list = list(
        [
            item
            for innerlist in CH4_staggered
            for item in innerlist
            if item not in seen and not seen.add(item)
        ]
    )
    CH4_staggered_messy = []
    for cname in CH4_list:
        CH4_staggered_messy = np.append(
            CH4_staggered_messy, CH4names[cname]["DLR"]
        )
    for x in CH4_staggered_messy:
        varlist.append(x)
    # Add the tracers from the map and timeseries:
    CH4_vars_map_messy = []
    for cname in CH4_vars_map:
        CH4_vars_map_messy = np.append(
            CH4_vars_map_messy, CH4names[cname]["DLR"]
        )
    for x in CH4_vars_map_messy:
        varlist.append(x)
    # Remove double entries:
    seen = set()
    varlist = [
        item for item in varlist if item not in seen and not seen.add(item)
    ]
    for var in varlist:
        if var.startswith("COSMO"):
            dicts_extracted.__setitem__(
                var,
                np.array(
                    fh_f.variables[var + "_f"][
                        (date_ext > flight_start) & (date_ext < flight_end)
                    ]
                ),
            )
            dicts_curtain.__setitem__(
                var,
                np.array(
                    fh_f.variables[var + "_c"][
                        (date_ext > flight_start) & (date_ext < flight_end)
                    ]
                ),
            )
        else:
            dicts_extracted.__setitem__(
                var,
                1e9
                * np.array(
                    fh_f.variables["tracer_gp_" + var + "_f"][
                        (date_ext > flight_start) & (date_ext < flight_end)
                    ]
                ),
            )
            dicts_curtain.__setitem__(
                var,
                1e9
                * np.array(
                    fh_f.variables["tracer_gp_" + var + "_c"][
                        (date_ext > flight_start) & (date_ext < flight_end)
                    ]
                ),
            )
    # Translate back:
    dicts_extracted_copy = dicts_extracted.copy()
    dicts_curtain_copy = dicts_curtain.copy()
    for item in dicts_extracted:
        fname = [x + "_f" for x in CH4names if CH4names[x]["DLR"] == item][0]
        cname = [x + "_c" for x in CH4names if CH4names[x]["DLR"] == item][0]
        dicts_extracted_copy["%s" % (fname)] = dicts_extracted_copy.pop(item)
        dicts_curtain_copy["%s" % (cname)] = dicts_curtain_copy.pop(item)
    dicts_curtain = np.array([dicts_curtain_copy])
    dicts_extracted_copy.__setitem__(
        "time",
        np.array(
            fh_f.variables["time"][
                (date_ext > flight_start) & (date_ext < flight_end)
            ]
        ),
    )
    dicts_extracted = np.array([dicts_extracted_copy])

    return dicts_curtain, dicts_extracted
