#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 09:43:22 2020

@author: stem
"""
#%%
import numpy as np

#%%
# Specify regular lat/lon-grid:
nx = 450
ny = 300
minx = 17.37
maxx = 31.0
miny = 42.82
maxy = 48.98
#%%
lons = np.linspace(minx, maxx, nx)
lats = np.linspace(miny, maxy, ny)
dx = lons[2] - lons[1]
dy = lats[2] - lats[1]

file1 = open("../data/grids/outgrid.txt", "w")

file1.write("# Grid for analysis output \n \n")
file1.write("gridtype = curvilinear \n")
file1.write("gridsize = %i \n" % (nx * ny))
file1.write("xsize = %i \n" % (nx))
file1.write("ysize = %i \n \n" % (ny))
file1.write("# Longitudes \n")
file1.write("xvals = ")
for ilat in lats:
    file1.write(" ".join(map(str, lons.astype(float))))
    file1.write("\n        ")
file1.write("\n")
file1.write("# Longitudes of cell corners \n")
file1.write("xbounds = \n")
counter = 0
for yy in lats:
    for xx in lons:
        counter += 1
        xbounds = np.array(
            [xx + 0.5 * dx, xx + 0.5 * dx, xx - 0.5 * dx, xx - 0.5 * dx]
        )
        file1.write(" ".join(map(str, xbounds.astype(float))))
        file1.write("\n")
file1.write("\n")
file1.write("# Latitudes \n")
file1.write("yvals = ")
for ilat, latval in enumerate(lats):
    file1.write(" ".join(map(str, np.full(nx, lats[ilat]).astype(float))))
    file1.write("\n        ")
file1.write("\n")
file1.write("# Latitudes of cell corners \n")
file1.write("ybounds = \n")
for yy in lats:
    for xx in lons:
        ybounds = np.array(
            [yy - 0.5 * dy, yy + 0.5 * dy, yy + 0.5 * dy, yy - 0.5 * dy]
        )
        file1.write(" ".join(map(str, ybounds.astype(float))))
        file1.write("\n")

file1.close()
