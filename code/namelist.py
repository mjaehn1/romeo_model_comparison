#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 31 22:31:11 2020
@author: stem
"""
#%%
from datetime import datetime, timedelta

#%%
__name__ = "namelist"
#%%
"""Pahts and directories:"""
flightdir = "../data/EMPA/extracted"
cosmooutput = "/output/COSMO-GHG/ROMEO/romeo-reanalysis-002/2019092700_0_744/cosmo_output_reduced"
cosmoextract = "../data/EMPA/extracted"
messyoutput = "../data/DLR/output"
messycurtain = "../data/DLR/extracted"
messyextract = "../data/DLR/extracted/interpolated"
wrfoutput = "../data/WRF/output"
wrfcurtian = "../data/WRF/extracted"
wrfextract = "../data/WRF/extracted"
plotdir = "../plots"
kml_path = "../data/locations/regions"
wellfile = "../data/locations/OMV/Sonde_latitude_longitude_OMV_Petrom_active_wells.xlsx"  # Excel-file with OMV-wells
facfile = "../data/locations/OMV/Facilities_OMV_Petrom_19022020.xlsx"  # Excel-file with OMV facilities
#%%
date_ref = datetime.strptime(
    "1970010100", "%Y%m%d%H"
)  # reference-date (01.01.1970)
#%% ROMEO-Domain:
pollon = -155.8
pollat = 44.0
startlon = -4.50
endlon = 4.48
startlat = -3.00
endlat = 2.98
#%% "Figure properties:
dpival = 500.0  # dpi-value for figure quality
minCm = 1.91  # min CH4 for maps and curtains
maxCm = 2.09  # max CH4 for maps and curtains
minCt = 1.91  # min CH4 for timeseries
maxCt = 2.13  # max CH4 for timeseries
minTm = -2.00  # min T for maps and curtains
maxTm = 12.00  # max T for maps and curtains
minTt = 0.00  # min T for timeseries
maxTt = 14.00  # max T for timeseries
minWm = -11.00  # min windspeed for maps and curtains
maxWm = 11.00  # max windspeed for maps and curtains
minWt = -11.00  # min windspeed for timeseries
maxWt = 7.00  # max windspeed for timeseries
margin = (
    0.11  # margin (degree lat/lon) around min/max of lat/lon of flight-path
)
dwind_t = 12  # frequency of wind-arrays along flight-track
dwind_m = 2  # frequency of wind-arrays on map
wscale = 10.0  # scale-length of wind array (inches); the smaller the number, the larger the wind array
wwidth = 0.034  # width of wind-array (inches)
tsize = 29.0  # size of text
scsize = 50.0  # size of scatter-points
frac = 0.083  # colorbar-fraction
pad = 0.330  # colorbar-pad
alpha = 0.3  # alpha-value for std-range in observations
#%%
"""Further variables:"""
# The names of the 21 XY-tracers:
all_XY = [
    "CH4_OMV01",
    "CH4_OMV02",
    "CH4_OMV03",
    "CH4_OMV04",
    "CH4_OMV05",
    "CH4_OMV06",
    "CH4_OMV07",
    "CH4_OMV08",
    "CH4_OMV09",
    "CH4_OMV10",
    "CH4_OMV11",
    "CH4_OMV12",
    "CH4_OMV13",
    "CH4_OMV14",
    "CH4_OMV15",
    "CH4_OMV16",
    "CH4_OMV17",
    "CH4_OMV18",
    "CH4_OMV19",
    "CH4_OMV20",
    "CH4_OMV21",
]
# Variables of the SA-flight:
SA_vars = [
    "CH4_obs",
    "CH4_std",
    "CO2_obs",
    "T_obs",
    "T_std",
    "U_obs",
    "U_std",
    "V_obs",
    "V_std",
    "flight_altitude",
    "RH_obs",
    "RH_std",
    "time",
]
# Variables of the INCAS-flight:
INCAS_vars = [
    "CH4_obs",
    "CH4_std",
    "CO2_obs",
    "flight_altitude_sl",
    "flight_altitude_gl",
    "H2O_obs",
    "H2O_std",
    "time",
]
# Variables of the curtains and extracted model data along flight:
model_vars_cosmo = [
    "T_f",
    "T_c",
    "U_f",
    "U_c",
    "V_f",
    "V_c",
]
model_vars_messy = [
    "COSMO_tm1",
    "COSMO_um1",
    "COSMO_vm1",
]
# List of kml-files (Polygons for Regions and Clusters):
kml_list = [
    "R2.kml",
    "R4.kml",
    "R5A.kml",
    "R6.kml",
    "R7.kml",
    "R8.kml",
    "Smaller_regions_cut.kml",
    "C6_east.kml",
    "C6_north.kml",
    "C7_northwest.kml",
]
#%%
"""Lookup-table for tracer names"""
CH4names = {
    "CH4_OMV01": {"DLR": "CH4OMV1", "LSCE": ""},
    "CH4_OMV02": {"DLR": "CH4OMV2", "LSCE": ""},
    "CH4_OMV03": {"DLR": "CH4OMV3", "LSCE": ""},
    "CH4_OMV04": {"DLR": "CH4OMV4", "LSCE": ""},
    "CH4_OMV05": {"DLR": "CH4OMV5", "LSCE": ""},
    "CH4_OMV06": {"DLR": "CH4OMV6", "LSCE": ""},
    "CH4_OMV07": {"DLR": "CH4OMV7", "LSCE": ""},
    "CH4_OMV08": {"DLR": "CH4OMV8", "LSCE": ""},
    "CH4_OMV09": {"DLR": "CH4OMV9", "LSCE": ""},
    "CH4_OMV10": {"DLR": "CH4OMV10", "LSCE": ""},
    "CH4_OMV11": {"DLR": "CH4OMV11", "LSCE": ""},
    "CH4_OMV12": {"DLR": "CH4OMV12", "LSCE": ""},
    "CH4_OMV13": {"DLR": "CH4OMV13", "LSCE": ""},
    "CH4_OMV14": {"DLR": "CH4OMV14", "LSCE": ""},
    "CH4_OMV15": {"DLR": "CH4OMV15", "LSCE": ""},
    "CH4_OMV16": {"DLR": "CH4OMV16", "LSCE": ""},
    "CH4_OMV17": {"DLR": "CH4OMV17", "LSCE": ""},
    "CH4_OMV18": {"DLR": "CH4OMV18", "LSCE": ""},
    "CH4_OMV19": {"DLR": "CH4OMV19", "LSCE": ""},
    "CH4_OMV20": {"DLR": "CH4OMV20", "LSCE": ""},
    "CH4_OMV21": {"DLR": "CH4OMV21", "LSCE": ""},
    "CH4_OMV": {"DLR": "CH4OMV", "LSCE": ""},
    "CH4_BG": {"DLR": "CH4_fx", "LSCE": ""},
    "CH4_EDF": {"DLR": "CH4EDF", "LSCE": ""},
    "CH4_GAS": {"DLR": "CH4GAS", "LSCE": ""},
    "CH4_GASPA": {"DLR": "CH4GASPAR", "LSCE": ""},
    "CH4_OIL": {"DLR": "CH4OIL", "LSCE": ""},
    "CH4_OILPA": {"DLR": "CH4OILPAR", "LSCE": ""},
    "CH4_OTHER": {"DLR": "CH4OTHERS", "LSCE": ""},
    "TNO_B": {"DLR": "CH4TNOB", "LSCE": ""},
    "TNO_D": {"DLR": "CH4TNOD", "LSCE": ""},
    "TNO_D_OMV": {"DLR": "TNODOMV", "LSCE": ""},
    "TNO_J": {"DLR": "CH4TNOJ", "LSCE": ""},
    "TNO_KL": {"DLR": "CH4TNOKL", "LSCE": ""},
    "CH4_LAKES": {"DLR": "CH4LAKES", "LSCE": ""},
    "T": {"DLR": "COSMO_tm1", "LSCE": ""},
    "U": {"DLR": "COSMO_um1", "LSCE": ""},
    "V": {"DLR": "COSMO_vm1", "LSCE": ""},
}
"""Lookup-table for model names"""
modellabel = {"EMPA": "COSMO", "DLR": "COSMO/Messy", "LSCE": "WRF"}
