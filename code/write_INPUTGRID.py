#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 09:43:22 2020

@author: stem
"""
#%%
import numpy as np
from netCDF4 import Dataset
import os

##Own functions:
from namelist import *

#%%
filename = "lffd2019102800.nc"
file = os.path.join(cosmodir, filename)
data = Dataset(file)

lon = np.array(data.variables["lon"][:])
lat = np.array(data.variables["lat"][:])
#%%
nx = lon.shape[1]
ny = lon.shape[0]
# lons=np.linspace(17.37,31.0,nx)
# lats=np.linspace(42.82,48.98,ny)
# dx = lons[2]-lons[1]
# dy = lats[2]-lats[1]

file1 = open("../data/grids/cosmo_in_grid.txt", "w")

file1.write("# Grid of COMSO output \n \n")
file1.write("gridtype = curvilinear \n")
file1.write("gridsize = %i \n" % (nx * ny))
file1.write("xsize = %i \n" % (nx))
file1.write("ysize = %i \n \n" % (ny))
file1.write("# Longitudes \n")
file1.write("xvals = ")
for ilat, latval in enumerate(lon[:, 0]):
    #    if ilat<3:
    #        print(" ".join(map(str, lon[ilat,:].astype(float))))
    #        print('**************************************************************')
    file1.write(" ".join(map(str, lon[ilat, :].astype(float))))
    file1.write("\n        ")
file1.write("\n")
file1.write("# Longitudes of cell corners \n")
file1.write("xbounds = \n")
counter = 0
for iy, yy in enumerate(lon[:, 0]):
    for ix, xx in enumerate(lon[iy, :]):
        counter += 1
        if xx != lon[iy, -1]:
            dx = 0.5 * (lon[iy, ix + 1] - xx)
        xbounds = np.array([xx + dx, xx + dx, xx - dx, xx - dx])
        #        if counter>2000 and counter<2020:
        #            print(xbounds)
        file1.write(" ".join(map(str, xbounds.astype(float))))
        file1.write("\n")
file1.write("\n")
file1.write("# Latitudes \n")
file1.write("yvals = ")
for ilat, latval in enumerate(lat[:, 0]):
    #    if ilat>30 and ilat<33:
    #        print(" ".join(map(str, lat[ilat,:].astype(float))))
    #        print('**************************************************************')
    file1.write(" ".join(map(str, lat[ilat, :].astype(float))))
    file1.write("\n        ")
file1.write("\n")
file1.write("# Latitudes of cell corners \n")
file1.write("ybounds = \n")
counter = 0
for iy, yy in enumerate(lat[:, 0]):
    for ix, xx in enumerate(lat[iy, :]):
        counter += 1
        if xx != lat[-1, ix]:
            dy = 0.5 * (lat[iy + 1, ix] - xx)
        ybounds = np.array([xx - dy, xx + dy, xx + dy, xx - dy])
        if counter > 20000 and counter < 20020:
            print(ybounds)
        file1.write(" ".join(map(str, xbounds.astype(float))))
        file1.write("\n")

file1.close()  # to change file access modes
