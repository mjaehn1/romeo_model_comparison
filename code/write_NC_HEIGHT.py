#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  3 14:12:22 2020
@author: stem
"""
#%%
import numpy as np
from netCDF4 import Dataset
import os

# Own functions:
from namelist import *

#%%
def create_nc_heights(map_height, model):
    print("---------------------------------------------------------")
    print("Creating height-files for interpolation...")
    targetheight = np.array([map_height])  # 34.33
    # Load constant-file:
    if model == "EMPA":
        infile = os.path.join(cosmooutput, "lffd2019092700c.nc")
        hhlfile = os.path.join(cosmooutput, "lffd2019092700c.nc")
    if model == "DLR":
        infile = os.path.join(
            messyoutput, "ROMEO_ANA_2____20191001_000000_tracer_gp.nc"
        )
        hhlfile = os.path.join(messycurtain, "hhl.nc")
    coord = Dataset(infile)
    height = Dataset(hhlfile)
    rlat = np.array(coord.variables["rlat"][:])
    rlon = np.array(coord.variables["rlon"][:])
    HHL = np.array(height.variables["HHL"][:])
    # Make HHL 3D and cut height levels for reduced output:
    if model == "EMPA":
        HHL = HHL[0, ...]
        HHL = HHL[::-1, ...][0:25, ...][::-1, ...]
    if model == "DLR":
        HHL = HHL[:-1, ...]
    # Create the height-variable:
    htarget = np.zeros((len(targetheight), len(rlat), len(rlon)))
    for ih in np.arange(htarget.shape[0]):
        htarget[ih, ...] = np.full((len(rlat), len(rlon)), targetheight[ih])
    # Define the 2 file names:
    filename = "../data/grids/nc_height.nc"
    filename2 = "../data/grids/hhl.nc"

    # Create the nc-file with the target-height:
    with Dataset(filename, mode="w") as ofile:
        olon = ofile.createDimension("rlon", len(rlon))
        olat = ofile.createDimension("rlat", len(rlat))
        olev = ofile.createDimension("level", len(targetheight))
        olon = ofile.createVariable("rlon", np.float32, ("rlon",))
        olat = ofile.createVariable("rlat", np.float32, ("rlat",))
        olev = ofile.createVariable("level", np.float32, ("level",))
        olon.axis = "X"
        olon.units = "degrees"
        olon.standard_name = "longitude"
        olat.axis = "Y"
        olat.units = "degrees"
        olat.standard_name = "latitude"
        olev.axis = "Z"
        olev.units = "m"
        olev.standard_name = "level"
        olon[:] = rlon
        olat[:] = rlat
        olev[:] = np.array([targetheight])
        units = "m"
        oh = ofile.createVariable(
            "height", np.float32, ("level", "rlat", "rlon")
        )
        oh.units = units
        oh[:] = htarget

    # Create the nc-file with the hhl-information:
    with Dataset(filename2, mode="w") as ofile:
        olon = ofile.createDimension("rlon", len(rlon))
        olat = ofile.createDimension("rlat", len(rlat))
        olev = ofile.createDimension("level", HHL.shape[0])
        olon = ofile.createVariable("rlon", np.float32, ("rlon",))
        olat = ofile.createVariable("rlat", np.float32, ("rlat",))
        olev = ofile.createVariable("level", np.float32, ("level",))
        olon.axis = "X"
        olon.units = "degrees"
        olon.standard_name = "longitude"
        olat.axis = "Y"
        olat.units = "degrees"
        olat.standard_name = "latitude"
        olev.axis = "Z"
        olev.units = "m"
        olev.standard_name = "level"
        olon[:] = rlon
        olat[:] = rlat
        olev[:] = np.arange(HHL.shape[0])
        units = "m"
        oh = ofile.createVariable("HHL", np.float32, ("level", "rlat", "rlon"))
        oh.units = units
        oh[:] = HHL
