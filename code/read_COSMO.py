#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 31 21:50:56 2020
@author: stem
"""
#%%
import numpy as np
from netCDF4 import Dataset
import os
import os.path
from datetime import timedelta
from cdo import Cdo

# Own functions:
from namelist import *

#%%
cdo = Cdo()
#%%
def readEMPA(map_height, date_obs, CH4_vars_map):

    ii = 0
    enddate = date_obs[-1]
    loopdate = date_obs[0]
    datelist = []
    while loopdate <= enddate:
        loopdate = (date_obs[0] + timedelta(minutes=30)).replace(
            minute=0, second=0
        ) + timedelta(hours=ii)
        if loopdate > (enddate + timedelta(minutes=30)):
            continue
        datelist = np.append(datelist, loopdate)
        ii += 1
    ###############
    print("---------------------------------------------------------")
    print("Remapping COMSO-output...")
    """Go through all timesteps and remap file with cdo"""
    for idate, filedate in enumerate(datelist):
        filename = "lffd%s.nc" % (filedate.strftime("%Y%m%d%H"))
        print("     ...%s" % (filedate.strftime("%Y%m%d%H")))
        file = os.path.join(cosmooutput, filename)
        # interpolate to specific height:
        cdo.intlevel3d(
            "../data/grids/hhl.nc %s ../data/grids/nc_height.nc" % (file),
            output="../data/grids/intheight.nc",
        )
        # remap to specific grid:
        cdo.remapcon(
            "../data/grids/outgrid.txt -selname,"
            + ",".join(map(str, np.append(CH4_vars_map, ["U", "V", "T", "QV"])))
            + " -setgrid,../data/grids/cosmo_in_grid.txt",
            input="../data/grids/intheight.nc",
            output="regridded%s.nc" % (filedate.strftime("%Y%m%d%H")),
        )
        os.remove("../data/grids/intheight.nc")
    ###############
    print("---------------------------------------------------------")
    print("Reading COMSO-output...")
    """Go through all timesteps and open/read file"""
    fields = []
    for idate, filedate in enumerate(datelist):
        filename = "regridded%s.nc" % (filedate.strftime("%Y%m%d%H"))
        print("     ...%s" % (filedate.strftime("%Y%m%d%H")))
        file = os.path.join(filename)
        data = Dataset(file)
        ###Read data
        CH4_field = np.zeros((data.variables["CH4_BG"][0, 0, ...]).shape)
        lat = data.variables["lat"][:]
        lon = data.variables["lon"][:]
        U = data.variables["U"][0, 0, ...]
        V = data.variables["V"][0, 0, ...]
        for xy_tracer in CH4_vars_map:
            ###correct for the wrong CH4_LAKES-values in our output:
            if xy_tracer == "CH4_LAKES":
                CH4_field += (0.001 / 4944753.17) * np.array(
                    data.variables["%s" % (xy_tracer)][0, 0, ...]
                )
            else:
                CH4_field += 0.001 * np.array(
                    data.variables["%s" % (xy_tracer)][0, 0, ...]
                )  ###ppb --> ppm
        os.remove(filename)
        fields = np.append(
            fields,
            {
                "CH4": CH4_field,
                "date": filedate,
                "lat": lat,
                "lon": lon,
                "U": U,
                "V": V,
            },
        )

    return fields


#%%
def readEXTRACTED_EMPA(
    model,
    flight_date,
    CH4_staggered,
    CH4_vars_map,
    mission,
    flight_start,
    flight_end,
    flight_nr,
):
    print("---------------------------------------------------------")
    print("Reading extraced model data...")
    infile1 = "_%s_%s_romeo-reanalysis-002_%s" % (
        mission,
        flight_date.strftime("%Y-%m-%d"),
        flight_date.strftime("%Y%m%d"),
    )
    infile2 = "_%s_%s_romeo-reanalysis-002_%s" % (
        mission,
        flight_date.strftime("%d%m%Y"),
        flight_date.strftime("%Y%m%d"),
    )
    if flight_date.strftime("%d%m%Y") == "03102019" and mission == "INCAS":
        infile2 = "_%s_%i-%s_romeo-reanalysis-002_%s" % (
            mission,
            flight_nr,
            flight_date.strftime("%d%m%Y"),
            flight_date.strftime("%Y%m%d"),
        )
    for flightfile in os.listdir(cosmoextract):
        if infile1 in flightfile or infile2 in flightfile:
            infile = flightfile

    fname = os.path.join(cosmoextract, infile)
    fh = Dataset(fname, mode="r")

    dicts_curtain = {}
    dicts_extracted = {}
    varlist = model_vars_cosmo
    ###Create date from epoch-time:
    ftime = np.array(fh.variables["time"][:])
    date_ext = []
    for isec in ftime:
        date_ext = np.append(date_ext, date_ref + timedelta(seconds=int(isec)))
    if not flight_start:
        flight_start = date_ext[0]
    if not flight_end:
        flight_end = date_ext[-1]
    # Add the tracers from the staggered plot:
    seen = set()
    CH4_list = list(
        [
            item
            for innerlist in CH4_staggered
            for item in innerlist
            if item not in seen and not seen.add(item)
        ]
    )
    for x in CH4_list:
        varlist.append(x + "_f")
        varlist.append(x + "_c")
    # Add the tracers from the map and timeseries:
    for x in CH4_vars_map:
        varlist.append(x + "_f")
        varlist.append(x + "_c")
    # Remove double entries:
    seen = set()
    varlist = [
        item for item in varlist if item not in seen and not seen.add(item)
    ]
    for var in varlist:
        if var.endswith("_f"):
            dicts_extracted.__setitem__(
                var,
                np.array(
                    fh.variables[var][
                        (date_ext > flight_start) & (date_ext < flight_end)
                    ]
                ),
            )
        if var.endswith("_c"):
            dicts_curtain.__setitem__(
                var,
                np.array(
                    fh.variables[var][
                        (date_ext > flight_start) & (date_ext < flight_end)
                    ]
                ),
            )
    dicts_curtain = np.array([dicts_curtain])
    dicts_extracted.__setitem__(
        "time",
        np.array(
            fh.variables["time"][
                (date_ext > flight_start) & (date_ext < flight_end)
            ]
        ),
    )
    dicts_extracted = np.array([dicts_extracted])

    return dicts_curtain, dicts_extracted
