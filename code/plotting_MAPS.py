#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 31 21:52:49 2020
@author: stem
"""
#%%
import matplotlib.pyplot as plt
import numpy as np
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.ticker as mticker
import os, errno
from namelist import *

#%%
def plotMAP(
    model_fields,
    dicts_flight,
    dicts_poly,
    dicts_omv,
    dicts_extracted,
    map_height,
    var_string,
    figname,
    model,
    mission,
):
    print("---------------------------------------------------------")
    print("Plotting map with fligh track...")
    ifig = 0
    for idate, plotdate in enumerate([x["date"] for x in model_fields]):
        print("     ...%s" % (plotdate.strftime("%Y%m%d%H")))
        ###Select flight track +/- 30 min of COSMO-output timestep:
        f_mask = np.zeros([x["lon"] for x in dicts_flight][0].shape[0])
        for ifl, fl_date in enumerate([x["date_obs"] for x in dicts_flight][0]):
            if fl_date >= (plotdate - timedelta(minutes=30)) and fl_date < (
                plotdate + timedelta(minutes=30)
            ):
                f_mask[ifl] = 1
        ###If the selected flight-track does not fall within +/- 30 min of this COSMO-timestep,
        ###...no map has to be plotted:
        if np.nanmax(f_mask) == 0:
            continue
        ###
        """Plot Figure"""
        fig = plt.figure(figsize=(23, 20))
        ax = plt.axes(projection=ccrs.PlateCarree())
        extent = [
            np.nanmin([x["lon"] for x in dicts_flight]) - margin,
            np.nanmax([x["lon"] for x in dicts_flight]) + margin,
            np.nanmin([x["lat"] for x in dicts_flight]) - margin,
            np.nanmax([x["lat"] for x in dicts_flight]) + margin,
        ]  # geographical range for maps
        ax.set_extent(extent)
        ######################
        # DRAW REGIONS/CLUSTERS
        for polyg in dicts_poly:
            xp, yp = polyg["polygon"].exterior.xy
            fullname = polyg["name"]
            halfname = str(fullname).split(">")[1]
            name = str(halfname).split("<")[0]
            color = list(
                np.random.choice(range(256), size=3) / 256
            )  # random color for every cluster
            ax.plot(xp, yp, alpha=0.95, color=color)
            if (
                xp[-1] > extent[0]
                and xp[-1] < extent[1]
                and yp[-1] > extent[2]
                and yp[-1] < extent[3]
            ):
                ax.text(
                    xp[-1],
                    yp[-1],
                    name,
                    color=color,
                    fontsize=tsize - 7,
                    transform=ccrs.PlateCarree(),
                )
        ###Draw CH4:
        fieldmap = ax.pcolormesh(
            [x["lon"] for x in model_fields][idate],
            [x["lat"] for x in model_fields][idate],
            [x["CH4"] for x in model_fields][idate],
            cmap="jet",
            vmin=minCm,
            vmax=maxCm,
            alpha=0.5,
            transform=ccrs.PlateCarree(),
        )
        ##############################
        # PLOT OMV WELLS AND FACILITIES
        wellsc = plt.scatter(
            [pt["rlon"] for pt in dicts_omv if pt["type1"] == "well"],
            [pt["rlat"] for pt in dicts_omv if pt["type1"] == "well"],
            s=int(0.8 * scsize),
            color="k",
            marker="^",
            transform=ccrs.PlateCarree(),
            label="OMV wells",
        )
        facilsc = plt.scatter(
            [pt["rlon"] for pt in dicts_omv if pt["type1"] == "facility"],
            [pt["rlat"] for pt in dicts_omv if pt["type1"] == "facility"],
            s=int(0.8 * scsize),
            color="k",
            marker="x",
            transform=ccrs.PlateCarree(),
            label="OMV facilities",
        )
        #############################
        # LINE FOR ENTIRE FLIGHT-TRACK
        track = ax.plot(
            [x["lon"] for x in dicts_flight][0],
            [x["lat"] for x in dicts_flight][0],
            color="k",
            lw=0.9,
            zorder=10,
            transform=ccrs.PlateCarree(),
            label="flight track",
        )
        ####################################
        # SCATTER PLOT OF MEASURED CH4-VALUES
        ax.scatter(
            [x["lon"] for x in dicts_flight][0][f_mask == 1],
            [x["lat"] for x in dicts_flight][0][f_mask == 1],
            c=[x["CH4_obs"] for x in dicts_flight][0][f_mask == 1],
            s=scsize + 180,
            vmin=minCm,
            vmax=maxCm,
            cmap="jet",
            zorder=20,
            transform=ccrs.PlateCarree(),
        )
        #################
        # PLOT WIND ARRAYS
        M = plt.quiver(
            [x["lon"] for x in dicts_flight][0][::dwind_t],
            [x["lat"] for x in dicts_flight][0][::dwind_t],
            [x["U_f"] for x in dicts_extracted][0][::dwind_t],
            [x["V_f"] for x in dicts_extracted][0][::dwind_t],
            scale_units="inches",
            units="inches",
            scale=wscale,
            width=wwidth,
            color="white",
            zorder=25,
        )
        Ma = plt.quiver(
            [x["lon"] for x in model_fields][idate][..., ::dwind_m, ::dwind_m],
            [x["lat"] for x in model_fields][idate][..., ::dwind_m, ::dwind_m],
            [x["U"] for x in model_fields][idate][..., ::dwind_m, ::dwind_m],
            [x["V"] for x in model_fields][idate][..., ::dwind_m, ::dwind_m],
            scale_units="inches",
            units="inches",
            scale=wscale,
            width=wwidth,
            color="white",
            label="Wind COMSO",
            zorder=25,
        )
        qka = ax.quiverkey(
            Ma,
            0.9,
            -0.1,
            4,
            r"$4 \frac{m}{s}$",
            labelpos="E",
            color="black",
            fontproperties={"size": tsize},
            transform=ax.transAxes,
        )
        if mission == "SA":
            Q = plt.quiver(
                [x["lon"] for x in dicts_flight][0][::dwind_t],
                [x["lat"] for x in dicts_flight][0][::dwind_t],
                [x["U_obs"] for x in dicts_flight][0][::dwind_t],
                [x["V_obs"] for x in dicts_flight][0][::dwind_t],
                scale_units="inches",
                units="inches",
                scale=wscale,
                width=wwidth,
                color="black",
                label="Wind OBS",
                zorder=30,
            )
        ##############
        # LAT/LON-TICKS
        gl = ax.gridlines(
            crs=ccrs.PlateCarree(),
            draw_labels=True,
            linewidth=2,
            color="gray",
            alpha=0.5,
            linestyle="--",
        )
        global axratio
        axratio = (extent[1] - extent[0]) / (extent[3] - extent[2])
        nrxticks = np.nanmax([3, int(3 * axratio)])
        nryticks = np.nanmax([3, int(3 / axratio)])
        gl.xlocator = mticker.FixedLocator(
            np.linspace(extent[0] + 0.01, extent[1] - 0.01, nrxticks).round(2)
        )
        gl.ylocator = mticker.FixedLocator(
            np.linspace(extent[2] + 0.01, extent[3] - 0.01, nryticks).round(2)
        )
        gl.xlabel_style = {"size": tsize, "color": "gray"}
        gl.ylabel_style = {"size": tsize, "color": "gray"}
        gl.xlabels_top = False
        gl.ylabels_right = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.text(
            -0.17,
            0.5,
            "latitude",
            va="bottom",
            ha="center",
            rotation="vertical",
            rotation_mode="anchor",
            fontsize=tsize,
            color="gray",
            transform=ax.transAxes,
        )
        ax.text(
            0.5,
            -0.11,
            "longitude",
            va="bottom",
            ha="center",
            rotation="horizontal",
            rotation_mode="anchor",
            fontsize=tsize,
            color="gray",
            transform=ax.transAxes,
        )
        #############################
        # TEXT: HEIGHT, DATE, CH4-VARS
        ax.text(
            0.95,
            0.95,
            "%.4i-%.2i-%.2i-%.2i:00"
            % (plotdate.year, plotdate.month, plotdate.day, plotdate.hour),
            va="bottom",
            ha="right",
            rotation="horizontal",
            rotation_mode="anchor",
            fontsize=tsize - 7,
            backgroundcolor="white",
            color="k",
            zorder=1000,
            transform=ax.transAxes,
        )
        ax.text(
            0.95,
            0.05,
            "height: %i m" % (map_height),
            va="bottom",
            ha="right",
            rotation="horizontal",
            rotation_mode="anchor",
            fontsize=tsize - 7,
            backgroundcolor="white",
            color="k",
            zorder=1000,
            transform=ax.transAxes,
        )
        ax.text(
            1.0,
            1.05,
            "%s" % (var_string),
            va="bottom",
            ha="right",
            rotation="horizontal",
            rotation_mode="anchor",
            fontsize=tsize - 7,
            color="k",
            transform=ax.transAxes,
        )
        ###########
        # ADD LEGEND
        ax.legend(
            fancybox=True,
            facecolor="oldlace",
            framealpha=0.85,
            fontsize=tsize - 7,
            loc="lower right",
            bbox_to_anchor=(0.0, 0.09),
        ).set_zorder(102)
        #############
        # ADD COLORBAR
        cbar = fig.colorbar(
            fieldmap, orientation="horizontal", shrink=0.7, pad=0.1
        )
        cbar.ax.locator_params(nbins=5)
        cbar.ax.tick_params(labelsize=tsize)
        cbar.set_label(
            "CH4 [ppm]", rotation=0, fontsize=tsize
        )  # labelpad=-40, y=1.05,
        ##############################
        # SPACE FOR COLORBAR AND LABELS
        plt.subplots_adjust(right=0.88)
        plt.subplots_adjust(left=0.18)
        plt.subplots_adjust(top=0.90)
        ############
        # SAVE FIGURE
        try:
            os.makedirs(figname)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
        ifig += 1
        fig.savefig(
            os.path.join(figname, "%s_%.2i.png" % (model, ifig)), dpi=dpival
        )
        fig.clf()
        plt.close()
